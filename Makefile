.PHONY: docs

test:
	python3 tests.py


publish:
	python3 setup.py register
	python3 setup.py sdist upload